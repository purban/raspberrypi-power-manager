package de.philipurban.pwrmanager;

import com.pi4j.io.gpio.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        final int ledOn_ms = 50;
        final int ledOff_ms = 3000;
        final int buttonStartDelay = 1000;
        MultiButton housingButton;
        MultiButton usbButton;
        int buttonCount = 0;
        int ledCount = 0;

        System.out.println("Execure Power Manager...");

        final GpioController gpio = GpioFactory.getInstance();

        final GpioPinDigitalOutput osLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, "OS LED", PinState.HIGH);
        final GpioPinDigitalOutput hddLED = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, "HDD LED", PinState.HIGH);
        final GpioPinDigitalInput shutdownButtonPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "Shutdown Button", PinPullResistance.PULL_UP);
//        final GpioPinDigitalInput rebootButtonPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, "Reboot Button", PinPullResistance.PULL_UP);
        final GpioPinDigitalInput usbShutdownSensorPin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_04, "USB Button", PinPullResistance.PULL_UP);

        housingButton = new MultiButton(osLED, MultiButton.Mode.DELAYED);
        usbButton = new MultiButton(hddLED, MultiButton.Mode.IMMEDIATELY);
        shutdownButtonPin.addListener(housingButton);
        usbShutdownSensorPin.addListener(usbButton);
//        rebootButtonPin.addListener(new RebootButton(hddLED));

        for(;;) {
            if(hddLED.getState().isHigh()){
                ledCount++;

                if(ledCount >= ledOn_ms) {
                    ledCount = 0;
                    hddLED.low();
                }
            } else {
                ledCount++;

                if(ledCount >= ledOff_ms) {
                    ledCount = 0;
                    hddLED.high();
                }
            }

            if(buttonCount < buttonStartDelay) {

                buttonCount++;
                if(buttonCount >= buttonStartDelay) {
                    housingButton.start();
                    usbButton.start();
                }
            }

            Thread.sleep(1);
        }
    }
}
