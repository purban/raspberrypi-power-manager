package de.philipurban.pwrmanager;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.io.IOException;

/**
 * @author Philip Urban
 */
public class MultiButton implements GpioPinListenerDigital {

    private GpioPinDigitalOutput statusLED;
    private Mode mode;
    private boolean started = false;
    private long buttonDown = 0;

    private final long restartPressTime_ms = 2000;

    public MultiButton(GpioPinDigitalOutput statusLED, Mode mode) {

        this.mode = mode;
        this.statusLED = statusLED;
    }

    @Override
    public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {

        if(!started) return;

        if(mode == Mode.DELAYED) {
            if(event.getState() == PinState.LOW) {
                buttonDown = System.currentTimeMillis();
            }

            if(buttonDown > 0 && event.getState() == PinState.HIGH) {
                if(System.currentTimeMillis() - buttonDown < restartPressTime_ms) {
                    shutdown();
                }
                else {
                    restart();
                }
            }
        }
        else if(mode == Mode.IMMEDIATELY) {

            if(event.getState() == PinState.LOW) {
                shutdown();
            }
        }
    }

    private void shutdown() {

        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("sudo initctl stop xbmc");
            runtime.exec("/opt/vc/bin/tvservice -o");
            runtime.exec("shutdown -h 0");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < 100; i++) {
            statusLED.toggle();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void restart() {

        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("sudo initctl stop xbmc");
            runtime.exec("/opt/vc/bin/tvservice -o");
            runtime.exec("shutdown -r 0");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < 100; i++) {
            statusLED.toggle();
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void start() {

        started = true;
    }

    public enum Mode {

        DELAYED,
        IMMEDIATELY
    }
}
