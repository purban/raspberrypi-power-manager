#!/bin/bash

echo "Updating project..."
git stash
git pull
echo "Clean compiling workspace..."
mvn clean -o
echo "Compiling..."
mvn install -o