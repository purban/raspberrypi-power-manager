#!/bin/bash

echo "Update apt-get..."
sudo apt-get update

echo "Install maven..."
sudo apt-get install maven

echo "Install Java Runtime Environment..."
sudo apt-get install oracle-java7-jdk

echo "Install GIT..."
sudo apt-get install git

echo "Install Pi4J..."
wget http://pi4j.googlecode.com/files/pi4j-1.0-SNAPSHOT.deb
sudo dpkg -i pi4j-1.0-SNAPSHOT.deb